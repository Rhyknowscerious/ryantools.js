function RyanTools() {

  // Static Properties
  RyanTools.version = 
    "20190611.0"
  ;
	   
  RyanTools.nl = 
    "\n"
  ;
  
  // Static Methods
  RyanTools.show = (
  	
  	 object,
  	 
  	 long = false
  	 
  ) => {
  	 
  	 if (long) {
  	 	
  	 	 flashLong(object)
  	 	 
  	 } else {
  	 	 
  	 	 flash(object)
  	 	 
  	 }
  	 
  }
  
  RyanTools.help = () => {
  	 
  	 RyanTools.show(
  	   `RyanTools.version${RyanTools.nl}` +
    	 `RyanTools.nl${RyanTools.nl}` +
  	   `RyanTools.help()${RyanTools.nl}` + 
  	   `RyanTools.show()${RyanTools.nl}` + 
  	   `RyanTools.Calc.getCardinalFromDeg()${RyanTools.nl}` + 
  	   
  	   `RyanTools.DateTime.get()${RyanTools.nl}` + 
  	   `RyanTools.DateTime.getString()${RyanTools.nl}` + 
  	   `RyanTools.DateTime.getLocaleString()${RyanTools.nl}` + 
  	   `RyanTools.DateTime.getLocaleDateString()${RyanTools.nl}` + 
  	   `RyanTools.DateTime.getLocaleTimeString()${RyanTools.nl}`
  	   
  	 ,
  	 true)
  	 
  }
  
  RyanTools.Calc = {
  	 
  	 getCardinalFromDeg: deg => {
  	   
  	   var unit = 11.25;

      var cardinal = "";

      if (
        deg < (1*unit) || 
        deg > ( 360 - (1*unit) )
      ) {

        cardinal = "N";

      } else if (
        deg >= (1*unit) && 
        deg <  (3*unit)
      ) {

        cardinal = "NNE";

      } else if (
        deg >= (3*unit) && 
        deg <  (5*unit)
      ) {

        cardinal = "NE";

      } else if (
        deg >= (5*unit) && 
        deg <  (7*unit)
      ) {

        cardinal = "ENE";

      } else if (
        deg >= (7*unit) && 
        deg <  (9*unit)
      ) {

        cardinal = "E";

      } else if (
        deg >= (9*unit) && 
        deg <  (11*unit)
      ) {

        cardinal = "ESE";

      } else if (
        deg >= (11*unit) && 
        deg <  (13*unit)
      ) {

        cardinal = "SE";

      } else if (
        deg >= (13*unit) && 
        deg <  (15*unit)
      ) {

        cardinal = "SSE";

      } else if (
        deg >= (15*unit) && 
        deg <  (17*unit)
      ) {

        cardinal = "S";

      } else if (
        deg >= (17*unit) && 
        deg <  (19*unit)
      ) {

        cardinal = "SSW";

      } else if (
        deg >= (19*unit) && 
        deg <  (21*unit)
      ) {

        cardinal = "SW";

      } else if (
        deg >= (21*unit) && 
        deg <  (23*unit)
      ) {

        cardinal = "WSW";

      } else if (
        deg >= (23*unit) && 
        deg <  (25*unit)
      ) {

        cardinal = "W";

      } else if (
        deg >= (25*unit) && 
        deg <  (27*unit)
      ) {

        cardinal = "WNW";

      } else if (
        deg >= (27*unit) && 
        deg <  (29*unit)
      ) {

        cardinal = "NW";

      } else if (
        deg >= (29*unit) && 
        deg <  (31*unit)
      ) {

        cardinal = "NNW";

      }
    
      return cardinal;
      
    }
  	 
  };
	 
	 // Nested Objects
	 RyanTools.DateTime = {
	 	 
	 	 get: () => 
	 	   new Date(),
    
    getString: () => 
      new Date().toString(),

    getLocaleString: () => 
      new Date().toLocaleString(),
    
    getLocaleDateString: () => 
      new Date().toLocaleDateString(),

    getLocaleTimeString: () => 
      new Date().toLocaleTimeString()
	 	
	 };
	 
}

new RyanTools();


