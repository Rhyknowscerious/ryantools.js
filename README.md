# README #

RyanTools is the name of one of my coding projects.

RyanTools is composed of libraries/modules/packages for languages I regularly use. 

RyanTools.js is the subset of modules for 
javascript. At the time of writing this 
readme, these two subsets exist.

RyanTools.Tasker.js
RyanTools.Wsh.js

I also have a PowerShell module and a C# library.

I plan on creating a package for Python as well.

## Anyways ##
### RyanTools.Web.js (coming soon) ###
Use a script tag to load into Web page:

<script 
    src="./wherever/RyanTools.Web.js" 
	type="text/javascript"
>
</script>

### RyanTools.Tasker.js ###
Save to phone then reference the filepath 
in the "Libraries" field on the "Javascript" 
Action or "JavaScriptlet" Action menu in 
any Task in any Project.

  or ...

reference the filepath in the "Path" field 
of a "JavaScript" Action.

Reference Example:

  wherever/RyanTools.Tasker.js

### RyanTools.Wsh.js ###

DISCLAIMER: I think this is how but I 
actually haven't checked so I might be 
wrong:

Create a wsf file "YourProject.wsf" with 
this in it:

<job id="YourProject">

   <script 
     language="JScript" 
     src="./wherever/RyanTools.Wsh.js"
   />
   
   <script language="JScript">
   
      RyanTools.show("Wazzzap wurld!?");
      
   </script>
   
</job>

