RyanTools = {};

RyanTools.Browser = {};

RyanTools.Browser.HTML = class {
    constructor(){}
    
    static getElements(by,searchText){
        let elements;
        switch(true) {
            case /id/i.test(by):
                elements = document.getElementById(searchText);
                break;
            case /class|classname/i.test(by):
                elements = document.getElementsByClassName(searchText);
                break;
            case /tag/i.test(by):
            default:
                elements = document.getElementByTagName(searchText);
        }
        return elements;
    }
    
    static newElement(tag,textNode,attributesMap){
        let element;
        element = document.createElement(
            tag
        );
        if (textNode) {
            element.appendChild(document.createTextNode(
                textNode
            ));
        }
        if (attributesMap) {
            attributesMap.forEach((value, key, map) =>{
                element.setAttribute(key,value);
            });
        }
        return element;
    }
}
